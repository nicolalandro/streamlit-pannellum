import { Streamlit } from "streamlit-component-lib"
import "pannellum"
import 'pannellum/build/pannellum.css'

function onRender(event) {
  const data = event.detail

  pannellum.viewer(
    'app',
    data.args.config
  );

  Streamlit.setFrameHeight('630')
}

Streamlit.events.addEventListener(Streamlit.RENDER_EVENT, onRender)
Streamlit.setComponentReady()
Streamlit.setFrameHeight()
